package de.kuschku.quasseldroid.util.listener

import de.kuschku.libquassel.protocol.BufferId
import de.kuschku.libquassel.protocol.NetworkId
import de.kuschku.quasseldroid.persistence.util.AccountId
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LinkClickHandler @Inject constructor() : LinkClickListener {
  private val listeners = mutableListOf<LinkClickListener>()
  fun register(listener: LinkClickListener) {
    listeners.add(listener)
  }

  fun unregister(listener: LinkClickListener) {
    listeners.remove(listener)
  }

  override fun openBuffer(
    bufferId: BufferId,
    accountId: AccountId?,
    forceJoin: Boolean
  ) {
    for (listener in listeners) {
      listener.openBuffer(
        bufferId, accountId, forceJoin
      )
    }
  }

  override fun openDirectMessage(
    networkId: NetworkId,
    nickName: String,
    forceJoin: Boolean
  ) {
    for (listener in listeners) {
      listener.openDirectMessage(
        networkId, nickName, forceJoin
      )
    }
  }

  override fun openChannel(
    networkId: NetworkId,
    channel: String,
    forceJoin: Boolean
  ) {
    for (listener in listeners) {
      listener.openChannel(
        networkId, channel, forceJoin
      )
    }
  }

  override fun openUrl(url: String) {
    for (listener in listeners) {
      listener.openUrl(url)
    }
  }
}
