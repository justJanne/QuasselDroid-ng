package de.kuschku.quasseldroid.util.listener

import dagger.Binds
import dagger.Module

@Module
interface ListenerModule {
  @Binds
  fun bindAutocompleteTextListener(handler: AutocompleteTextHandler): AutocompleteTextListener

  @Binds
  fun bindlinkClickListener(handler: LinkClickHandler): LinkClickListener
}
