package de.kuschku.quasseldroid.util.listener

import de.kuschku.libquassel.protocol.BufferId
import de.kuschku.libquassel.protocol.NetworkId
import de.kuschku.quasseldroid.persistence.util.AccountId

class QuasselLinkClickListener(
  private val wrapped: LinkClickListener,
  private val onClickQuasselLink: () -> Unit,
) : LinkClickListener {
  override fun openBuffer(bufferId: BufferId, accountId: AccountId?, forceJoin: Boolean) {
    wrapped.openBuffer(bufferId, accountId, forceJoin)
    onClickQuasselLink()
  }

  override fun openDirectMessage(networkId: NetworkId, nickName: String, forceJoin: Boolean) {
    wrapped.openDirectMessage(networkId, nickName, forceJoin)
    onClickQuasselLink()
  }

  override fun openChannel(networkId: NetworkId, channel: String, forceJoin: Boolean) {
    wrapped.openChannel(networkId, channel, forceJoin)
    onClickQuasselLink()
  }

  override fun openUrl(url: String) = wrapped.openUrl(url)
}
