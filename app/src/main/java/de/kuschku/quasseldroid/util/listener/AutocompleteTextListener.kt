package de.kuschku.quasseldroid.util.listener

interface AutocompleteTextListener {
  fun autocompleteText(
    text: CharSequence,
    suffix: String?
  )
}
