package de.kuschku.quasseldroid.util.listener

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AutocompleteTextHandler @Inject constructor() : AutocompleteTextListener {
  private val listeners = mutableListOf<AutocompleteTextListener>()
  fun register(listener: AutocompleteTextListener) {
    listeners.add(listener)
  }

  fun unregister(listener: AutocompleteTextListener) {
    listeners.remove(listener)
  }

  override fun autocompleteText(
    text: CharSequence,
    suffix: String?,
  ) {
    for (listener in listeners) {
      listener.autocompleteText(text, suffix)
    }
  }
}
