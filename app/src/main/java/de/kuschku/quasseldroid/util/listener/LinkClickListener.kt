package de.kuschku.quasseldroid.util.listener

import de.kuschku.libquassel.protocol.BufferId
import de.kuschku.libquassel.protocol.NetworkId
import de.kuschku.quasseldroid.persistence.util.AccountId

interface LinkClickListener {
  fun openBuffer(
    bufferId: BufferId,
    accountId: AccountId? = null,
    forceJoin: Boolean = false
  )

  fun openDirectMessage(
    networkId: NetworkId,
    nickName: String,
    forceJoin: Boolean = false
  )

  fun openChannel(
    networkId: NetworkId,
    channel: String,
    forceJoin: Boolean = false
  )

  fun openUrl(url: String)
}
