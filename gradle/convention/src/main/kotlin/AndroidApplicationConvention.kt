/*
 * Quasseldroid - Quassel client for Android
 *
 * Copyright (c) 2024 Janne Mareike Koschinski
 * Copyright (c) 2024 The Quassel Project
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3 as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import com.android.build.api.dsl.ApplicationExtension
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.BasePluginExtension
import org.gradle.kotlin.dsl.configure
import util.git
import java.util.*

class AndroidApplicationConvention : Plugin<Project> {
  override fun apply(target: Project) {
    with(target) {
      with(pluginManager) {
        apply("com.android.application")
        apply("justjanne.kotlin.android")
        apply("justjanne.signing")
      }

      val commit = git("rev-parse", "HEAD").orNull
      val code = git("rev-list", "--count", "HEAD", "--tags").orNull
      val name = git("describe", "--always", "--tags", "HEAD").orNull

      extensions.configure<BasePluginExtension> {
        if (name == null) {
          archivesName.set("${rootProject.name}")
        } else {
          archivesName.set("${rootProject.name}-$name")
        }
      }

      extensions.configure<ApplicationExtension> {
          compileSdk = 35

        defaultConfig {
          minSdk = 21
            targetSdk = 35

          applicationId = "${rootProject.group}.${rootProject.name.lowercase(Locale.ROOT)}"

          versionCode = code?.toIntOrNull()
          versionName = name

          val fancyVersionName = if (commit == null || name == null) name
          else "<a href=\\\"https://git.kuschku.de/justJanne/QuasselDroid-ng/commit/$commit\\\">$name</a>"

          buildConfigField("String", "GIT_HEAD", "\"${git("rev-parse", "HEAD").getOrElse("")}\"")
          buildConfigField("String", "FANCY_VERSION_NAME", "\"${fancyVersionName ?: ""}\"")
          buildConfigField("long", "GIT_COMMIT_DATE", "${git("show", "-s", "--format=%ct").getOrElse("0")}L")

          signingConfig = signingConfigs.findByName("default")

          // Disable test runner analytics
          testInstrumentationRunnerArguments["disableAnalytics"] = "true"
        }

        buildFeatures {
          buildConfig = true
        }

        compileOptions {
          sourceCompatibility = JavaVersion.VERSION_11
          targetCompatibility = JavaVersion.VERSION_11
        }

        testOptions {
          unitTests.isIncludeAndroidResources = true
        }

        packaging {
          resources {
            excludes.add("/META-INF/*.md")
          }
        }

        lint {
          warningsAsErrors = true
          lintConfig = file("../lint.xml")
        }
      }
    }
  }
}
