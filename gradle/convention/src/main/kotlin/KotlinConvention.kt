/*
 * Quasseldroid - Quassel client for Android
 *
 * Copyright (c) 2024 Janne Mareike Koschinski
 * Copyright (c) 2024 The Quassel Project
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3 as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.api.tasks.testing.Test
import org.gradle.jvm.toolchain.JavaLanguageVersion
import org.gradle.kotlin.dsl.configure
import org.gradle.kotlin.dsl.provideDelegate
import org.gradle.kotlin.dsl.withType
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

class KotlinConvention : Plugin<Project> {
  override fun apply(target: Project) {
    with(target) {
      with(pluginManager) {
        apply("org.jetbrains.kotlin.jvm")
        apply("com.google.devtools.ksp")
        apply("org.jetbrains.kotlin.plugin.serialization")
      }

      // Use withType to workaround https://youtrack.jetbrains.com/issue/KT-55947
      tasks.withType<KotlinCompile>().configureEach {
        compilerOptions {
          // Set JVM target to 11
          jvmTarget.set(JvmTarget.JVM_11)
          // Treat all Kotlin warnings as errors (disabled by default)
          // Override by setting warningsAsErrors=true in your ~/.gradle/gradle.properties
          val warningsAsErrors: String? by target
          allWarningsAsErrors.set(warningsAsErrors.toBoolean())
          freeCompilerArgs.add(
            "-opt-in=kotlin.ExperimentalUnsignedTypes"
          )
        }
      }

      tasks.withType<Test> {
        useJUnitPlatform()
      }

      configure<JavaPluginExtension> {
        // Up to Java 11 APIs are available through desugaring
        // https://developer.android.com/studio/write/java11-minimal-support-table
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11

        toolchain {
          languageVersion.set(JavaLanguageVersion.of(17))
        }
      }
    }
  }
}
