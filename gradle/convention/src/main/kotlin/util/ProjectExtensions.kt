/*
 * Quasseldroid - Quassel client for Android
 *
 * Copyright (c) 2024 Janne Mareike Koschinski
 * Copyright (c) 2024 The Quassel Project
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3 as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package util

import org.gradle.api.Project
import org.gradle.api.provider.Provider
import java.util.*


fun Project.git(vararg command: String): Provider<String> =
  providers.exec { commandLine("git", *command) }
    .standardOutput
    .asText
    .map { it.trim() }

fun Project.properties(fileName: String): Provider<Properties> =
  providers.fileContents(rootProject.layout.projectDirectory.file(fileName))
    .asBytes
    .map { Properties().apply { load(it.inputStream()) } }
