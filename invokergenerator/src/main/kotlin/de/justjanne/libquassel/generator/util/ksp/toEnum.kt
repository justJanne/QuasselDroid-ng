/*
 * libquassel
 * Copyright (c) 2024 Janne Mareike Koschinski
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

package de.justjanne.libquassel.generator.util.ksp

import com.google.devtools.ksp.symbol.KSType
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.asClassName
import kotlin.reflect.KClass

internal inline fun <reified T : Enum<T>> KSType.toEnum(): T? {
    return asClassName().toEnum(T::class)
}

internal inline fun <reified T : Enum<T>> ClassName.toEnum(): T? {
    return toEnum(T::class)
}

internal fun <T : Enum<T>> ClassName.toEnum(clazz: KClass<T>): T? {
  val enumClassName = clazz.asClassName()
    return clazz.java.enumConstants.find {
    this.canonicalName == enumClassName.nestedClass(it.name).canonicalName
  }
}
